CXX=g++
CXXFLAGS=-std=c++1y -Wall -Wextra -Werror -pedantic
SRCFILES=main.cc
SRCDIR=src/
SRC=$(addprefix $(SRCDIR), $(SRCFILES))
OBJS=$(SRC:.cc=.o)
EXEC=procgen

all: proceduralgeneration
	

proceduralgeneration: $(OBJS)
	$(CXX) $(OBJS) -o $(EXEC)

clean:
	rm -rf */*.o */*.swp $(EXEC)
