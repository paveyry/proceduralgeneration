/**
 ** \file perlindd.hh
 ** \brief Declaration of perlin2d::Perlin2D
 */

#ifndef PERLIN2D_HH
#define PERLIN2D_HH

namespace perlin2d
{
  /// \brief A set of useful static functions for Perlin Noise implementation
  class Perlin2D
  {
    public:
      /// Get Pi
      static double pi();
      /// Linear Interpolation implementation
      static double linear_interpolation(double a, double b, double x);
      /// Cosine Interpolation implementation
      static double cosine_interpolation(double a, double b, double x);
      /// Cubic Interpolation implementation
      static double cubic_interpolation(double v0, double v1, double v2,
          double v3, double x);
      /// Basic random noise function
      static double basic_noise(int x, int y);
      /// Smoothed random noise function
      static double smoothed_noise(double x, double y);
    private:
      /// Private constructor
      Perlin2D();
      /// Private destructor
      ~Perlin2D();
  };
}

#endif /* !PERLIN2D_HH */
