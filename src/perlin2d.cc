#include <cmath>
#include "perlin2d.hh"

namespace perlin2d
{
  double Perlin2D::pi()
  {
    return std::atan(1) * 4;
  }

  double Perlin2D::linear_interpolation(double a, double b, double x)
  {
    return a * (1 - x) + b * x;
  }

  double Perlin2D::cosine_interpolation(double a, double b, double x)
  {
    double d = (1 - std::cos(x * Perlin2D::pi())) * 0.5;
    return a * (1 - d) + b * d;
  }

  double Perlin2D::cubic_interpolation(double v0, double v1, double v2,
      double v3, double x)
  {
    double p = (v3 - v2) - (v0 - v1);
    double q = (v0 - v1) - p;
    double r = v2 - v0;
    return p * x * x * x + q * x * x + r * x + v1;
  }

  double Perlin2D::basic_noise(int x, int y)
  {
    int n = x + y * 57;
    n = (n << 13) ^ n;
    return 1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff)
        / 1073741824.0;
  }

  double Perlin2D::smoothed_noise(double x, double y)
  {
    double corners = (basic_noise(x - 1, y - 1) + basic_noise(x + 1, y - 1)
        + basic_noise(x - 1, y + 1) + basic_noise(x + 1, y + 1)) / 16;
    double sides = (basic_noise(x - 1, y) + basic_noise(x + 1, y)
        + basic_noise(x, y - 1) + basic_noise(x, y + 1)) / 8;
    double center = basic_noise(x, y) / 4;
    return corners + sides + center;
  }
}
